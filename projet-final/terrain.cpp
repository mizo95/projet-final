#include "pch.h"
#include "terrain.h"

const int number_line = 20;
const int number_column = 20;
char matrice[number_line][number_column];
robot robotx;
int finishC = 0;
int finishL = 0;

terrain::terrain()
{
}


terrain::~terrain()
{
}

void terrain::initMatrice() {

	for (int i = 0; i < number_line; i++)
	{
		for (int j = 0; j < number_column; j++)
		{
			matrice[i][j] = '0';
		}
	}
}
void terrain::setDepartPosition() {
	for (int i = number_line - 1, k = 0; i >= 0; i--, k++)
	{
		for (int j = 0; j < number_column; j++)
		{
			switch (matrice[i][j])
			{
			case 'd': robotx.setPositionColumn(j); robotx.setPositionLine(k); break;
			case 'a': finishC = j; finishL = k; break;
			}
		}
	}
}

  void terrain::redim(int x, int y)
{
	glViewport(0.0, 0.0, x, y);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 20.0, 0.0, 20.0);


}
  void terrain::drawlab(void)
  {
	  glColor3d(0.3, 0.4, 0.8);
	  //mur
	  glBegin(GL_QUADS);
	  for (int i = number_line - 1, k = 0; i >= 0; i--, k++)
		  for (int j = 0; j <= number_column; j++)
			  if (matrice[i][j] == '0')
			  {
				  glVertex2d(j, k);
				  glVertex2d(j + 1, k);
				  glVertex2d(j + 1, k + 1);
				  glVertex2d(j, k + 1);
			  }
	  glEnd();

	  //point d'arriver du robot
	  glPushMatrix();
	  glTranslated(finishC + 0.5, finishL + 0.5, 0.0);
	  glColor3d(0.0, 0.0, 0.0);

	  for (double c = 0.0; c < 1.0; c += 0.2)
		  glutWireCube(c);

	  glPopMatrix();
  }


 void terrain::affichage(void)
{
	glClearColor(3.0, 3.0, 3.0, 31.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	//code labyrinth
	drawlab();
	robotx.drawRobot();
	glutSwapBuffers();
	glFlush();
}



void terrain::loadLab(std::string f)
{
	std::fstream file;
	file.open(f);
	if (!file)
	{
		std::cout << " Fichier introuvable!" << std::endl;
		system("pause");
		exit(1);

	}
	initMatrice();
	for (int i = 0; i < number_line; i++)
	{
		for (int j = 0; j < number_column; j++)
		{
			file >> matrice[i][j];
		}
	}
	setDepartPosition();

	file.close();
}

void drawlab(void)
{
	glColor3d(0.3, 0.4, 0.8);
	//mur
	glBegin(GL_QUADS);
	for (int i = number_line - 1, k = 0; i >= 0; i--, k++)
		for (int j = 0; j <= number_column; j++)
			if (matrice[i][j] == '0')
			{
				glVertex2d(j, k);
				glVertex2d(j + 1, k);
				glVertex2d(j + 1, k + 1);
				glVertex2d(j, k + 1);
			}
	glEnd();

	//point d'arriver du robot
	glPushMatrix();
	glTranslated(finishC + 0.5, finishL + 0.5, 0.0);
	glColor3d(0.0, 0.0, 0.0);

	for (double c = 0.0; c < 1.0; c += 0.2)
		glutWireCube(c);

	glPopMatrix();
}





void terrain::victory()
{
	if ((robotx.getPositionColumn() == finishC) && (robotx.getPositionLine() == finishL))
	{
		std::cout << "Victoire !" << std::endl;
		affichage();
		system("pause");
		exit(1);
	}
}


void terrain::moveRobot(int button, int x, int y)
{

	switch (button)
	{
	case GLUT_KEY_DOWN: {robotx.moveByRightHandAlgo(); break; }
	case 2: {robotx.moveByPledge(); break; }


	default:
		break;
	}
	victory();
	glutPostRedisplay();
}




	void terrain::graphique(void)
	{
		glutInitWindowPosition(300, 100);
		glutInitWindowSize(500, 500);
		glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE);
		glutCreateWindow("Labyrinthe Qualit�");
		glutReshapeFunc(redim);
		glutDisplayFunc(affichage);
		glutSpecialFunc(moveRobot);
		loadLab("mur.txt");
		glutMainLoop();
	}

