#include "pch.h"
#include "algorithme.h"
#include "terrain.h"

int arriveeColonne;
int arriveeLigne;

algorithme::algorithme(robot robotx)
{
	this->robotx = robotx;
}


algorithme::~algorithme()
{
}

robot algorithme::robotX()
{
	return robotx;
}

//D�placer le robot selon la main droite
void algorithme::deplacerRobotSelonLaMainDroite()
{
	int hasard = rand() % 2;

	switch (hasard)
	{
	case 0:
		robotX().moveRight();
		break;
	case 1:
		robot().moveLeft();
		break;
	}
	terrain::victory();
	glutPostRedisplay();
}

void algorithme::deplacerRobotSeleonPledge()
{
	int compteur = 0;
	while ((robotX().getPositionColumn() != arriveeColonne) && (robotX().getPositionLine() != arriveeLigne))
	{
		int hasard = rand() % 3;
		switch (hasard)
		{
		case 0:
			robotX().moveUp();
			break;
		case 1:
			if (compteur != 0)
				robotX().moveLeft();
			compteur++;
			break;
		case 2:
			if (compteur != 0)
				robotX().moveRight();
			compteur--;
			break;
		}
	}
	terrain::victory();
	glutPostRedisplay();
}
