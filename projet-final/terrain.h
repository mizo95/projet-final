#pragma once
#include "pch.h"
#include <stdlib.h>
#include "GL/glut.h"

#include "Robot.h"

#include <iostream>
#include <fstream>
#include <string>
class terrain
{

public:
	terrain();
	~terrain();
	 int getFinshColonne();
	 int getFinishLine();
	static void affichage();
	void initMatrice();
	void setDepartPosition();
	void graphique();
	static void redim(int x, int y);
	void loadLab(std::string f);
	static void drawlab(void);
	static void moveRobot(int button, int x, int y);
	static void victory();
};

