#include "pch.h"
#include "robot.h"

extern const int number_line = 20;
extern const int number_column = 20;
extern char matrice[number_line][number_column];

robot::robot(int pl, int pc):position_line(pl), position_column(pc)
{}

robot::robot()
{
	position_line   = 0;
	position_column = 0;
}

int robot::getPositionLine() const
{
	return position_line;
}

int robot::getPositionColumn() const
{
	return position_column;
}

void robot::setPositionLine(int pl)
{
	position_line = pl;
}

void robot::setPositionColumn(int pc)
{
	position_column = pc;
}

void robot::drawRobot()
{
	glPushMatrix();
	glTranslated(position_column + 0.5, position_line + 0.5, 0.0);
	glColor3d(1.0, 1.0, 1.0);
	glutSolidCube(0.6);
	glColor3d(1.0, 1.0, 0.0);
	glTranslated(0.2, 0.2, 0.0);
	glutSolidCube(0.6);
	glTranslated(-0.4, 0.0, 0.0);
	glutSolidCube(0.6);
	glPopMatrix();
}
bool robot::isObstacleFront() 
{
	if (matrice[(number_line - 2) - position_line][position_column] != '0')
		return false;
	return true;
}

bool robot::isObstacleBehind()
{if(matrice[number_line - position_line][position_column] != '0')
	return  false;

	return true;
}

bool robot::isObstacleLeft() 
{

	if (matrice[(number_line - 1) - position_line][position_column - 1] != '0')
	return  false;
	

	return true;
}

bool robot::isObstacleRight()
{
	if (matrice[(number_line - 1) - position_line][position_column + 1] != '0')
		return false;
	
	return true;

}

void robot::moveUp()
{
	if((position_line < number_line - 1)&& isObstacleFront()== false)
	position_line++;
}

void robot::moveDown()
{
	if((position_line > 0) && isObstacleBehind()==false)
	position_line--;
}

void robot::moveLeft()
{
	if((position_column > 0) && isObstacleLeft()==false)
	position_column--;
}

void robot::moveRight()
{
	if((position_column < number_column -1) && isObstacleRight()==false)
	position_column++;
}


void robot::moveByRightHandAlgo()

{
	

	//while(//il nest pas arriv� a la position d'arrive�)
		
	while (this->getPositionColumn()!=0 && this->getPositionLine()!=0)
		{
			if (isObstacleRight() == false)
			{
				moveRight();
				this->getPositionColumn();
				this->getPositionLine();
			}
			else if (isObstacleFront() == false)
			{

				moveUp();
				this->getPositionColumn();
				this->getPositionLine();
			}
			else if( isObstacleLeft() == false) 
			
			{
				moveLeft();
				this->getPositionColumn();
				this->getPositionLine();
			}
	   else {

	moveDown();
	this->getPositionColumn();
	this->getPositionLine();

            }	
			


			}



		}



	void robot::moveByPledge()
	{
		 


	}