#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "pch.h"
#include <stdlib.h>
#include "GL/glut.h"
#include "Robot.h"
#include "algorithme.h"

using namespace std;



class afficheur
{
public:
	afficheur();
	static void graphique();
	static void redim(int x, int y);
	static void affiche();
	static void drawlab(void);
	static void moveRobot(int button, int x, int y);
	static void loadLab(string f);
	static void victory();
	~afficheur();
};

