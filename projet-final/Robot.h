#pragma once
#ifndef ROBOT_H
#define ROBOT_H
#include <cstdlib>
#include <ctime>
#include "GL/glut.h"

class robot
{
public:
	robot(int pl, int pc);
	robot();
	int getPositionLine() const;
	int getPositionColumn() const;

	void setPositionLine(int pl);
	void setPositionColumn(int pc);

	void drawRobot();

	void moveUp();
	void moveDown();
	void moveLeft();
	void moveRight();
	bool isObstacleFront();
	bool isObstacleBehind();
	bool isObstacleLeft();
	bool isObstacleRight();

	void moveByRightHandAlgo();
	void moveByPledge();

private:
	int position_column;
	int position_line;
   
};

#endif