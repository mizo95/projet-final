#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include "GL\glut.h"
#include "robot.h"

using std::fstream;
using std::cout;
using std::endl;
using std::vector;

class algorithme
{

private:
	robot robotx;
public:
	algorithme(robot robotx);
	~algorithme();
	void deplacerRobotSelonLaMainDroite();
	void deplacerRobotSeleonPledge();
	robot robotX();
};

